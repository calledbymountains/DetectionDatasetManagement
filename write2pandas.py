import pandas as pd

def write2pandas(annotations, hdf_name, hdf_key, append=False):
    """
    Writes a set of records to dask dataframe.
    :param annotations: List of dictionaries.
    :param df_name: Full path to HDF file holding the dataframe
    :param hdf_key: Key with which the dataframe will be identified.
    :return: None
    """
    frame = pd.DataFrame(annotations)
    print(set(frame['aspect_ratio'].tolist()))
    frame.to_hdf(hdf_name, key=hdf_key, format='table', append=append)
    return None
